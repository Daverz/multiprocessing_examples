from multiprocessing.managers import BaseManager, current_process

# an example of some existing class you want to use from another process
class TestADC(object):

    def startAcquisition(self):
        import time
        return time.time()


class Proxy(object):
    """Proxy for ADC calls"""

    def initializeADC(self):
        self.adc = TestADC()

    def startAcquisition(self):
        print 'Starting acquisition in:', current_process()
        return self.adc.startAcquisition()


class SimpleManager(BaseManager):
    pass

SimpleManager.register('Proxy', Proxy)

if __name__=='__main__':
    manager = SimpleManager()
    manager.start()
    proxy = manager.Proxy()
    proxy.initializeADC()
    print 'Started at time:', proxy.startAcquisition()