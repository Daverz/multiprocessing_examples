import time
from multiprocessing import Process, Queue, current_process, Array
from threading import Thread

class TestADC(object):

    def startAcquisition(self):
        return time.time()


def messageloop(qmessage, qinfo, data):
    while True:
        message = qmessage.get() # block until we get a message
        if 'initialize' in message:
            klass = message[1]
            adc = klass()
        elif 'start' in message:
            print 'Starting acquisition in:', current_process()
            t = adc.startAcquisition()
            qinfo.put(t)
        elif 'stop' in message:
            break

if __name__=='__main__':
    qmessage = Queue()
    qinfo = Queue()
    data = Array(10, 'b')
    # process = Process(target=messageloop, args=(qmessage, qinfo))
    process = Thread(target=messageloop, args=(qmessage, qinfo, data))
    process.start()
    qmessage.put( ('initialize', TestADC) )
    qmessage.put('start')
    t = qinfo.get()
    print 'Started at time:', t
    qmessage.put('stop')
    process.join()